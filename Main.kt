fun main(args: Array<String>) {
    var valueReaded: String = readLine()!!
    try {
        println(getSqrt(valueReaded.toDouble()))
    } catch (e: NumberFormatException) {
        println("Value isn't a number")
    } catch (e: ArithmeticException) {
        println("Value is less than 0")
    } catch (e: Exception) {
        println(e.message)
    } finally {
        println("Program end")
    }
}

fun getSqrt(arg: Double): Double {
    if (arg < 0.0) {
        throw ArithmeticException()
    }
    return Math.sqrt(arg)
}
